from .app import app
from flask import render_template
from .models import get_sample
from .models import get_author
from flask_wtf import FlaskForm
from wtforms import StringField, HiddenField, PasswordField
from wtforms.validators import DataRequired
from flask import url_for, redirect
from .commands import *
from .app import db
from .models import Author, User, Book
from hashlib import sha256
from flask_login import login_user, current_user, logout_user
from flask import request, flash

@app.route("/")
def home():
    return render_template("home.html",title="Liste des 100 meilleurs livres",books=get_sample())

class AuthorForm(FlaskForm):
    id=HiddenField('id')
    name=StringField('Nom',validators=[DataRequired()])

@app.route("/edit/author/<int:id>")
def edit_author(id):
    a=get_author(id)
    f=AuthorForm(id=a.id,name=a.name)
    return render_template("edit-author.html",author=a,form=f)

@app.route("/save/author/", methods=("POST",))
def save_author():
    a=None
    f=AuthorForm()
    if f.validate_on_submit():
        id=int(f.id.data)
        a=get_author(id)
        a.name=f.name.data
        db.session.commit()
        return redirect(url_for('edit_author',id=a.id))
    a=get_author(int(f.id.data))
    return render_template("edit-author.html",author=a,form=f)

class LoginForm(FlaskForm):
    username=StringField('Username')
    password=PasswordField('Password')

    def get_authenticated_user(self):
        user=User.query.get(self.username.data)
        if user is None :
            return None
        m=sha256()
        m.update(self.password.data.encode())
        passwd=m.hexdigest()
        return user if passwd==user.password else None

class InscriptionForm(FlaskForm):
    username=StringField('Username')
    password=PasswordField('Password')
    confirmpass=PasswordField('Confirm password')

    def add_user(self):
        m=sha256()
        m.update(self.password.data.encode())
        user=User(username=self.username.data,password=m.hexdigest())
        if (User.query.get(self.username.data)!=None):
            return None
        db.session.add(user)
        db.session.commit()
        return user

@app.route("/login/",methods=("GET","POST",))
def login():
    f=LoginForm()
    error = None
    if f.validate_on_submit():
        user=f.get_authenticated_user()
        if user:
            login_user(user)
            return redirect(url_for("home"))
        else:
            error = "Mot de passe ou login incorrect !"
    return render_template("login.html",error=error,form=f)

@app.route("/inscription/",methods=("GET","POST",))
def inscription():
    f=InscriptionForm()
    error = None
    if f.password.data == f.confirmpass.data:
        if f.validate_on_submit():
            user=f.add_user()
            if user:
                return redirect(url_for("home"))
            else:
                error = "Username déjà existant"
        return render_template("inscription.html",error=error,form=f)
    error = 'Mot de passe et confirmation non similaire'
    return render_template("inscription.html",error=error,form=f)

@app.route("/logout/")
def logout():
    logout_user()
    return redirect(url_for('home'))
