from flask import Flask
from flask_login import LoginManager
app=Flask(__name__)
app.config["BOOTSTRAP_SERVE_LOCAL"] = True
app.config["SQLALCHEMY_DATABASE_URI"] = True
import os.path
def mkpath(p):
    return os.path.normpath(
        os.path.join(
            os.path.dirname(__file__),
            p))
from flask_sqlalchemy import SQLAlchemy
app.config['SQLALCHEMY_DATABASE_URI']=(
    'sqlite:///'+mkpath('../myapp.db'))
app.config['SECRET_KEY']="7c754d63-0d3a-4ef4-954a-d30120a40618"
login_manager=LoginManager(app)
db=SQLAlchemy(app)
