import yaml, os.path
from .app import db, login_manager
from flask_login import UserMixin

class Author(db.Model):
    __table_args__ = {'extend_existing': True}

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100))
    def __repr__(self):
        return "%s" % (self.name)

class Book(db.Model):
    __table_args__ = {'extend_existing': True}
    
    id = db.Column(db.Integer,primary_key=True)
    price = db.Column(db.Float)
    title = db.Column(db.String(100))
    url = db.Column(db.String(200))
    img = db.Column(db.String(100))
    author_id=db.Column(db.Integer, db.ForeignKey("author.id"))
    author = db.relationship("Author",backref=db.backref("books",lazy="dynamic"))
    def __repr__(self):
        return "<Book (%d) %s>" % (self.id,self.title)

class User(db.Model, UserMixin):
    __table_args__ = {'extend_existing': True}

    username=db.Column(db.String(50),primary_key=True)
    password=db.Column(db.String(64))
    def get_id(self):
        return self.username

def get_sample(debut=0,nb=1000):
    return Book.query.offset(debut).limit(nb).all()

def get_author(authorid):
    return Author.query.get_or_404(authorid)

@login_manager.user_loader
def load_user(username):
    return User.query.get(username)
