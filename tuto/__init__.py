from .app import app
from .app import db
import tuto.views
import tuto.commands
import tuto.models
from flask_bootstrap import Bootstrap
Bootstrap(app)
